# se ejecuta el script para las inserciones DEMO=TRUE
@reboot  /mnt/ssd/METVOD-APP-DEPLOY/scripts/Demo.sh && echo "Ejecutado demo - $(date)"

#(intento de sincronización del versionamiento del programa a las 24hrs).
0 0 * * *  /mnt/ssd/METVOD-APP-DEPLOY/scripts/Update.sh && echo "Ejecutado update - $(date)"
#(intento de sincronización del versionamiento del programa a las 12hrs).
0 12 * * *  /mnt/ssd/METVOD-APP-DEPLOY/scripts/Update.sh  && echo "Ejecutado update - $(date)"

#(intento de sincronización del versionamiento del programa a las 19hrs).
0 19 * * *  /mnt/ssd/METVOD-APP-DEPLOY/scripts/Update.sh && echo "Ejecutado update - $(date)"

#(borrar la caché de las imágenes de Docker cada que se reinicia el bus).
@reboot sleep 300 && docker rmi $(docker images -a -q) && echo "Ejecutado Borrado de imagenes cache docker - $(date)"
