#!/bin/sh
#Se pasa el backup al contenedor
docker cp ../backup/backup.tar postgres:backup.tar

#Se restaura la base de datos DeveVOD-APP
docker exec postgres pg_restore -d DeveVOD-APP -Ft backup.tar -h 127.0.0.1 -p 5432 -U postgres
