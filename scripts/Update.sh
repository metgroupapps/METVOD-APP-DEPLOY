#!/bin/sh
cd /mnt/ssd/METVOD-APP-DEPLOY
git checkout .
git pull
docker-compose pull nginx
docker-compose pull frontend
docker-compose pull backend
docker-compose up -d nginx
docker-compose up -d frontend
docker-compose up -d backend
docker-compose restart backend
# docker rmi $(docker images -a -q)
chmod 755 -R /mnt/ssd/METVOD-APP-DEPLOY/scripts
cd /mnt/ssd/METVOD-APP-DEPLOY/scripts
