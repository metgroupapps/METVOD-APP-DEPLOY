#!/bin/sh

while [ "$DEMO" = "true" ]
do
  # Comando 1
  docker exec postgres psql -U postgres -d DeveVOD-APP -c "UPDATE \"VOD\".\"VEHICLE_TRACKINGS\" SET \"LATITUDE\" = '4.8083138651125745', \"LONGITUDE\" = '-75.82492695096428', \"GEOZONE_ID\" = 2416, \"CHECKPOINT\" = 1, \"ROUTE_ID\" = 6, \"VEHICLE_ID\" = 11 WHERE \"ID\" = 20;" &

  # Esperar 30 segundos
  sleep 30

  # Comando 2
  docker exec postgres psql -U postgres -d DeveVOD-APP -c "UPDATE \"VOD\".\"VEHICLE_TRACKINGS\" SET \"LATITUDE\" = '4.802903', \"LONGITUDE\" = '-75.83022', \"GEOZONE_ID\" = 2417, \"CHECKPOINT\" = 1, \"ROUTE_ID\" = 6, \"VEHICLE_ID\" = 11 WHERE \"ID\" = 20;" &

  # Esperar 30 segundos
  sleep 30

  # Comando 3
  docker exec postgres psql -U postgres -d DeveVOD-APP -c "UPDATE \"VOD\".\"VEHICLE_TRACKINGS\" SET \"LATITUDE\" = '4.810510136351254', \"LONGITUDE\" = '-75.7949217618108', \"GEOZONE_ID\" = 2418, \"CHECKPOINT\" = 1, \"ROUTE_ID\" = 6, \"VEHICLE_ID\" = 11 WHERE \"ID\" = 20;" &

  # Esperar 30 segundos
  sleep 30

  # Comando 4
  docker exec postgres psql -U postgres -d DeveVOD-APP -c "UPDATE \"VOD\".\"VEHICLE_TRACKINGS\" SET \"LATITUDE\" = '4.805979012718262', \"LONGITUDE\" = '-75.75240632965352', \"GEOZONE_ID\" = 2419, \"CHECKPOINT\" = 1, \"ROUTE_ID\" = 6, \"VEHICLE_ID\" = 11 WHERE \"ID\" = 20;" &

  sleep 30

done
